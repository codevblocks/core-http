/*
 * Http.java
 *
 * Created on Oct 21, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http;

public final class Http {

    /* ======================== */
    /* HTTP Methods             */
    /* ======================== */

    /** HTTP GET method */
    public static final String GET                                          = "GET";
    /** HTTP POST method */
    public static final String POST                                         = "POST";
    /** HTTP PUT method */
    public static final String PUT                                          = "PUT";
    /** HTTP DELETE method */
    public static final String DELETE                                       = "DELETE";
    /** HTTP HEAD method */
    public static final String HEAD                                         = "HEAD";
    /** HTTP PATCH method */
    public static final String PATCH                                        = "PATCH";
    /** HTTP OPTIONS method */
    public static final String OPTIONS                                      = "OPTIONS";

    /* ======================== */
    /* HTTP status codes        */
    /* ======================== */

    /** HTTP OK status code */
    public static final int OK                                              = 200;
    /** HTTP Created status code */
    public static final int CREATED                                         = 201;
    /** HTTP No Content status code */
    public static final int NO_CONTENT                                      = 204;
    /** HTTP Bad Request status code */
    public static final int BAD_REQUEST                                     = 400;
    /** HTTP Unauthorized status code */
    public static final int UNAUTHORIZED                                    = 401;
    /** HTTP Forbidden status code */
    public static final int FORBIDDEN                                       = 403;
    /** HTTP Not Found status code */
    public static final int NOT_FOUNT                                       = 404;
    /** HTTP Method Not Allowed status code */
    public static final int METHOD_NOT_ALLOWED                              = 405;
    /** HTTP Conflict status code */
    public static final int CONFLICT                                        = 409;
    /** HTTP Precondition Failed status code */
    public static final int PRECONDITION_FAILED                             = 412;
    /** HTTP Unsupported Media Type status code */
    public static final int UNSUPPORTED_MEDIA_TYPE                          = 415;
    /** HTTP Internal Server Error status code */
    public static final int INTERNAL_SERVER_ERROR                           = 500;

    /* ======================== */
    /* CHARSETS                 */
    /* ======================== */

    /** "ISO-8859-1" charset */
    public static final String CHARSET_ISO_8859_1                           = "ISO-8859-1";
    /** "UTF-8" charset */
    public static final String CHARSET_UTF_8                                = "UTF-8";

    /* ======================== */
    /* HTTP headers             */
    /* ======================== */

    /** "Accept-Encoding" header */
    public static final String HEADER_ACCEPT_ENCODING                       = "Accept-Encoding";
    /** "Authorization" header */
    public static final String HEADER_AUTHORIZATION                         = "Authorization";
    /** "Content-Disposition" header */
    public static final String HEADER_CONTENT_DISPOSITION                   = "Content-Disposition";
    /** "Content-Encoding" header */
    public static final String HEADER_CONTENT_ENCODING                      = "Content-Encoding";
    /** "Content-Length" header */
    public static final String HEADER_CONTENT_LENGTH                        = "Content-Length";
    /** "Content-Type" header */
    public static final String HEADER_CONTENT_TYPE                          = "Content-Type";
    /** "Cookie" header */
    public static final String HEADER_COOKIE                                = "Cookie";
    /** "ETag" header */
    public static final String HEADER_ETAG                                  = "ETag";
    /** "If-Match" header */
    public static final String HEADER_IF_MATCH                              = "If-Match";
    /** "Last-Modified" header */
    public static final String HEADER_LAST_MODIFIED                         = "Last-Modified";
    /** "Nonce" header */
    public static final String HEADER_NONCE                                 = "Nonce";
    /** "Set-Cookie" header */
    public static final String HEADER_SET_COOKIE                            = "Set-Cookie";
    /** "User-Agent" header */
    public static final String HEADER_USER_AGENT                            = "User-Agent";
    /** "Host" header */
    public static final String HEADER_HOST                                  = "Host";

    /* ======================== */
    /* HTTP header params       */
    /* ======================== */

    /** "boundary" header param */
    public static final String HEADER_PARAM_BOUNDARY                        = "boundary";
    /** "charset" header param */
    public static final String HEADER_PARAM_CHARSET                         = "charset";
    /** "filename" header param */
    public static final String HEADER_PARAM_FILENAME                        = "filename";
    /** "name" header param */
    public static final String HEADER_PARAM_NAME                            = "name";

    /* ======================== */
    /* HTTP media types         */
    /* ======================== */

    /** "application/json" media type */
    public static final String MEDIA_TYPE_APP_JSON                          = "application/json";
    /** "application/octet-stream" type */
    public static final String MEDIA_TYPE_APP_OCTET_STREAM                  = "application/octet-stream";
    /** "application/x-www-form-urlencoded" media type */
    public static final String MEDIA_TYPE_APP_FORM_URL_ENCODED              = "application/x-www-form-urlencoded";
    /** "image/jpeg" media type */
    public static final String MEDIA_TYPE_IMAGE_JPEG                        = "image/jpeg";
    /** "image/png" media type */
    public static final String MEDIA_TYPE_IMAGE_PNG                         = "image/png";
    /** "multipart/form-data" media type */
    public static final String MEDIA_TYPE_MULTIPART_FORM_DATA               = "multipart/form-data";
    /** "text/plain" media type */
    public static final String MEDIA_TYPE_TEXT_PLAIN                        = "text/plain";

    /* ============================ */
    /* Content-Disposition values   */
    /* ============================ */

    /** Content-Disposition: attachment */
    public static final String CONTENT_DISPOSITION_ATTACHMENT               = "attachment";
    /** Content-Disposition: form-data */
    public static final String CONTENT_DISPOSITION_FORM_DATA                = "form-data";
    /** Content-Disposition: inline */
    public static final String CONTENT_DISPOSITION_INLINE                   = "inline";

    /* ======================== */
    /* HTTP encodings           */
    /* ======================== */

    public static final String ENCODING_GZIP                                = "gzip";

    /* ======================== */
    /* HTTP authorizations      */
    /* ======================== */

    /** "Basic" authorization */
    public static final String AUTHORIZATION_BASIC                          = "Basic";
    /** "Bearer" authorization */
    public static final String AUTHORIZATION_BEARER                         = "Bearer";

}
