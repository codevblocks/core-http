/*
 * HttpJsonObjectRequest.java
 *
 * Created on Jan 9, 2020
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class HttpJsonRequest<T, E> extends HttpStringRequest<T, E> {

    public HttpJsonRequest(final String method, final String path) {
        super(method, path);
    }

    @Override
    protected HttpResponse<T, E> parseResponse(final HttpRawResponse response, final String stringContent) throws Throwable {
        final char startChar = stringContent.charAt(0);

        if (startChar == '{') {
            final JSONObject jsonObjectContent = new JSONObject(stringContent);
            return parseResponse(response, jsonObjectContent);
        } else if (startChar == '[') {
            final JSONArray jsonArrayContent = new JSONArray(stringContent);
            return parseResponse(response, jsonArrayContent);
        }

        return new HttpResponse<>(response);
    }

    protected HttpResponse<T, E> parseResponse(HttpRawResponse response, JSONObject jsonObject) throws Throwable {
        return new HttpResponse<>(response);
    }

    protected HttpResponse<T, E> parseResponse(HttpRawResponse response, JSONArray jsonArray) throws Throwable {
        return new HttpResponse<>(response);
    }

}
