/*
 * HttpLog.java
 *
 * Created on Mar 19, 2014
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http;

import com.codevblocks.core.http.auth.HttpAuth;
import com.codevblocks.core.http.parameter.HttpParameter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

public class HttpLog {

    private static final class SingletonHolder {
        private static final HttpLog INSTANCE = new HttpLog();
    }

    private final DateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private boolean mEnabled;
    private StringBuilder mBuffer;

    private HttpLog() {
        mEnabled = false;
    }

    public static final HttpLog getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public final synchronized void setEnabled(final boolean enabled) {
        if (mEnabled != enabled) {
            mEnabled = enabled;
            mBuffer = (mEnabled ? new StringBuilder() : null);
        }
    }

    public final synchronized void clear() {
        if (mEnabled) {
            mBuffer.delete(0, mBuffer.length());
        }
    }

    public final synchronized String getData() {
        return (mBuffer != null ? mBuffer.toString() : null);
    }

    public final synchronized void log(final HttpRequest request) {
        if (mEnabled && request != null) {
            mBuffer.append("=========== REQUEST ===========").append("\r\n");

            mBuffer.append(String.format("Timestamp: %1$s", mDateFormat.format(new Date()))).append("\r\n");
            mBuffer.append(String.format("Path: %1$s", request.getPath())).append("\r\n");
            mBuffer.append(String.format("Method: %1$s", request.getMethod())).append("\r\n");

            final Collection<? extends HttpParameter> parameters = request.getParameters();
            if (parameters != null && parameters.size() > 0) {
                for (HttpParameter parameter : parameters) {
                    mBuffer.append(String.format("Parameter: %1$s == %2$s", parameter.name, parameter.value)).append("\r\n");
                }
            }

            final HttpAuth auth = request.getAuthorization();
            if (auth != null) {
                mBuffer.append(String.format("Auth: %1$s (%2$s)", auth.getType().name(), auth.getClass().getSimpleName())).append("\r\n");
            }

            final Map<String, String> headers = request.getHeaders();
            final Set<Map.Entry<String, String>> headerEntries = (headers != null ? headers.entrySet() : null);
            final int headersCount = (headerEntries != null ? headerEntries.size() : 0);
            if (headersCount > 0) {
                int i = 0;
                for (Map.Entry<String, String> headerEntry : headerEntries) {
                    mBuffer.append(String.format("Header[%1$d]: %2$s == %3$s", i, headerEntry.getKey(), headerEntry.getValue())).append("\r\n");
                }
            }

            final byte[] data = request.getData();
            if (data != null && data.length > 0) {
                final String contentType = (headers != null ? headers.get(Http.HEADER_CONTENT_TYPE) : null);
                if (contentType == null || !contentType.startsWith(Http.MEDIA_TYPE_MULTIPART_FORM_DATA)) {
                    mBuffer.append(String.format("Data: \r\n %1$s", new String(data))).append("\r\n");
                }
            }

            mBuffer.append("========= END REQUEST =========").append("\r\n");
        }
    }

    public final synchronized void log(final HttpRawResponse response) {
        if (mEnabled && response != null) {
            mBuffer.append("=========== RESPONSE ===========").append("\r\n");

            mBuffer.append(String.format("Timestamp: %1$s", mDateFormat.format(new Date()))).append("\r\n");

            mBuffer.append(String.format("Code: %1$d", response.getCode())).append("\r\n");
            mBuffer.append(String.format("Message: %1$s", response.getMessage())).append("\r\n");

            final Map<String, String> headers = response.getHeaders();
            final Set<Map.Entry<String, String>> headerEntries = (headers != null ? headers.entrySet() : null);
            final int headersCount = (headerEntries != null ? headerEntries.size() : 0);
            if (headersCount > 0) {
                int i = 0;
                for (Map.Entry<String, String> headerEntry : headerEntries) {
                    mBuffer.append(String.format("Header[%1$d]: %2$s == %3$s", i++, headerEntry.getKey(), headerEntry.getValue())).append("\r\n");
                }
            }

            final byte[] data = response.getData();
            if (data != null && data.length > 0) {
                final String contentType = (headers != null ? headers.get(Http.HEADER_CONTENT_TYPE) : null);
                if (contentType == null || !contentType.startsWith("image/")) {
                    mBuffer.append(String.format("Data: \r\n %1$s", new String(data))).append("\r\n");
                }
            }

            mBuffer.append("========= END RESPONSE =========").append("\r\n");
        }
    }

}