/*
 * HttpRawResponse.java
 *
 * Created on Oct 21, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http;

import java.util.Map;

public class HttpRawResponse {

    private int mCode;
    private String mMessage;
    private byte[] mData;
    private Map<String, String> mHeaders;

    public HttpRawResponse() {
        this.mCode = 0;
        this.mMessage = null;
        this.mData = null;
        this.mHeaders = null;
    }

    public int getCode() {
        return mCode;
    }

    public void setCode(int code) {
        this.mCode = code;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        this.mMessage = message;
    }

    public byte[] getData() {
        return mData;
    }

    public void setData(byte[] data) {
        this.mData = data;
    }

    public Map<String, String> getHeaders() {
        return mHeaders;
    }

    public void setHeaders(Map<String, String> headers) {
        this.mHeaders = headers;
    }

    public boolean isSuccessful() {
        final int codeClass = getCode() / 100;
        return (codeClass != 4 && codeClass != 5);
    }

}
