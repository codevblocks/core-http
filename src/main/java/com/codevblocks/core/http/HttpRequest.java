/*
 * HttpRequest.java
 *
 * Created on Oct 16, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http;

import com.codevblocks.core.http.auth.HttpAuth;
import com.codevblocks.core.http.executor.HttpRequestExecutor;
import com.codevblocks.core.http.executor.HttpRequestExecutorFactory;
import com.codevblocks.core.http.parameter.HttpParameter;

import javax.net.ssl.TrustManager;
import java.util.Collection;
import java.util.Map;

public abstract class HttpRequest<T, E> {

    private final String mMethod;
    private final String mPath;

    private long mBytesSent;
    private long mBytesReceived;

    public HttpRequest(final String method, final String path) {
        this.mMethod = method;
        this.mPath = path;

        this.mBytesSent = 0;
        this.mBytesReceived = 0;
    }

    public final String getMethod() {
        return mMethod;
    }

    public final String getPath() {
        return mPath;
    }

    public Collection<? extends HttpParameter> getParameters() {
        return null;
    }

    public int getConnectionTimeout() {
        return 0;
    }

    public int getReadTimeout() {
        return 0;
    }

    public Map<String, String> getHeaders() {
        return null;
    }

    public byte[] getData() {
        return null;
    }

    public HttpAuth getAuthorization() {
        return null;
    }

    public TrustManager[] getTrustManagers() {
        return null;
    }

    public boolean shouldRetry(final HttpResponse<T, E> response, final Throwable t) {
        return false;
    }

    public int getRetryTimeout(final HttpResponse<T, E> response, final Throwable t) {
        return 0;
    }

    protected HttpRequestExecutor getExecutor() {
        return HttpRequestExecutorFactory.getDefault().newExecutor(this);
    }

    public HttpResponse<T, E> execute() throws Throwable {
        HttpResponse<T, E> response = null;
        Throwable throwable = null;

        try {
            response = parseResponse(getExecutor().execute());
        } catch (Throwable t) {
            throwable = t;
        }

        if (response == null || !response.isSuccessful()) {
            if (shouldRetry(response, throwable)) {
                Thread.sleep(getRetryTimeout(response, throwable));
                response = execute();
            }
        }

        if (throwable != null) {
            throw throwable;
        }

        return response;
    }

    protected abstract HttpResponse<T, E> parseResponse(final HttpRawResponse response) throws Throwable;

    public long getBytesSent() {
        return mBytesSent;
    }

    public void setBytesSent(final long bytesSent) {
        this.mBytesSent = bytesSent;
    }

    public long getBytesReceived() {
        return mBytesReceived;
    }

    public void setBytesReceived(final long bytesReceived) {
        this.mBytesReceived = bytesReceived;
    }

}
