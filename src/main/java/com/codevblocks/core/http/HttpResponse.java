/*
 * HttpResponse.java
 *
 * Created on Oct 16, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http;

public class HttpResponse<T, E> extends HttpRawResponse {

    private final T mObject;
    private final E mError;

    public HttpResponse(final HttpRawResponse rawResponse) {
        this(rawResponse, null, null);
    }

    public HttpResponse(final HttpRawResponse rawResponse, final T object) {
        this(rawResponse, object, null);
    }

    public HttpResponse(final HttpRawResponse rawResponse, final T object, final E error) {
        setCode(rawResponse.getCode());
        setMessage(rawResponse.getMessage());
        setData(rawResponse.getData());
        setHeaders(rawResponse.getHeaders());
        this.mObject = object;
        this.mError = error;
    }

    @Override
    public boolean isSuccessful() {
        return super.isSuccessful() && mError == null;
    }

    public T getObject() {
        return mObject;
    }

    public E getError() {
        return mError;
    }

}
