/*
 * HttpStringRequest.java
 *
 * Created on Jan 8, 2020
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http;

import com.codevblocks.core.http.util.HttpUtil;

import java.util.Map;

public abstract class HttpStringRequest<T, E> extends HttpRequest<T, E> {

    private static final String DEFAULT_CHARSET = Http.CHARSET_UTF_8;

    public HttpStringRequest(final String method, final String path) {
        super(method, path);
    }

    @Override
    protected HttpResponse<T, E> parseResponse(final HttpRawResponse response) throws Throwable {
        final byte[] responseData = response.getData();

        if (responseData != null && responseData.length > 0) {
            final Map<String, String> responseHeaders = response.getHeaders();
            final String contentTypeHeader = responseHeaders != null ? responseHeaders.get(Http.HEADER_CONTENT_TYPE) : null;
            final String contentTypeCharset = HttpUtil.parseHeaderParamValue(contentTypeHeader, Http.HEADER_PARAM_CHARSET);

            final String responseString = new String(responseData, contentTypeCharset != null ? contentTypeCharset : DEFAULT_CHARSET);

            return parseResponse(response, responseString);
        }

        return new HttpResponse<>(response);
    }

    protected abstract HttpResponse<T, E> parseResponse(HttpRawResponse response, String stringContent) throws Throwable;

}
