/*
 * HttpAuth.java
 *
 * Created on Nov 26, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.auth;

public interface HttpAuth {

    public enum Type {
        BASIC
    }

    Type getType();

    HttpAuthCredentials getCredentials();

}
