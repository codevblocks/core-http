/*
 * HttpBasicAuth.java
 *
 * Created on Nov 26, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.auth;

public class HttpBasicAuth implements HttpAuth {

    private final HttpBasicAuthCredentials mCredentials;

    public HttpBasicAuth(final String username, final String password) {
        this.mCredentials = new HttpBasicAuthCredentials(username, password);
    }

    @Override
    public Type getType() {
        return Type.BASIC;
    }

    @Override
    public HttpAuthCredentials getCredentials() {
        return mCredentials;
    }

}
