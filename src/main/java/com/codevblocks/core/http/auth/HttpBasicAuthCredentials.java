/*
 * HttpBasicAuthCredentials.java
 *
 * Created on Nov 26, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.auth;

public class HttpBasicAuthCredentials implements HttpAuthCredentials {

    private final String mUsername;
    private final String mPassword;

    public HttpBasicAuthCredentials(final String username, final String password) {
        this.mUsername = username;
        this.mPassword = password;
    }

    public String getName() {
        return mUsername;
    }

    public String getPassword() {
        return mPassword;
    }

}
