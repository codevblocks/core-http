package com.codevblocks.core.http.data;

import com.codevblocks.core.http.Http;
import com.codevblocks.core.http.io.MeteredOutputStream;
import com.codevblocks.core.http.io.VoidOutputStream;
import com.codevblocks.core.http.util.HttpUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MultipartBody {

    private static final byte[] DATA_COLON_SPACE = new byte[] { ':', ' ' };
    private static final byte[] DATA_CRLF = new byte[] { '\r', '\n' };
    private static final byte[] DATA_DASH_DASH = new byte[] { '-', '-' };

    private final String mBoundary;

    private final ArrayList<Part> mParts;

    public MultipartBody(final String boundary) {
        this.mBoundary = boundary;
        this.mParts = new ArrayList<>();
    }

    public void addPart(final Part part) {
        mParts.add(part);
    }

    public final String getBoundary() {
        return mBoundary;
    }

    public final long getSize() throws IOException {
        return writeData(new VoidOutputStream(), true);
    }

    public long writeData(final OutputStream outputStream) throws IOException {
        return writeData(outputStream, false);
    }

    private long writeData(final OutputStream outputStream, final boolean count) throws IOException {
        final MeteredOutputStream out = new MeteredOutputStream(outputStream);

        final byte[] boundaryData = mBoundary.getBytes(Http.CHARSET_UTF_8);

        for (Part part : mParts) {
            out.write(DATA_DASH_DASH);
            out.write(boundaryData);
            out.write(DATA_CRLF);

            if (part.headers != null && part.headers.size() > 0) {
                for (Map.Entry<String, String> header : part.headers.entrySet()) {
                    out.write(header.getKey().getBytes(Http.CHARSET_UTF_8));
                    out.write(DATA_COLON_SPACE);
                    out.write(header.getValue().getBytes(Http.CHARSET_UTF_8));
                    out.write(DATA_CRLF);
                }
            }

            out.write(DATA_CRLF);

            if (count) {
                long partDataSize = part.getDataSize();

                int writeCount;
                while (partDataSize > 0) {
                    writeCount = (int) (partDataSize > Integer.MAX_VALUE ? Integer.MAX_VALUE : partDataSize);
                    out.write(null, 0, writeCount);
                    partDataSize -= writeCount;
                }
            } else {
                part.writeData(out);
            }

            out.write(DATA_CRLF);
        }

        out.write(DATA_DASH_DASH);
        out.write(boundaryData);
        out.write(DATA_DASH_DASH);
        out.write(DATA_CRLF);

        return out.getWriteCount();
    }

    public static abstract class Part {

        private final Map<String, String> headers;

        Part(final Map<String, String> headers) {
            this.headers = headers;
        }

        public abstract long getDataSize();
        public abstract void writeData(OutputStream out) throws IOException;

    }

    public static abstract class FormDataPart extends Part {

        FormDataPart(final Map<String, String> headers) {
            super(headers);
        }

        public static abstract class Builder<T extends FormDataPart> {

            protected String name;
            protected String mediaType;

            public Builder() {
                this.name = null;
                this.mediaType = null;
            }

            public final Builder<T> setName(final String name) {
                this.name = name;
                return this;
            }

            public final Builder<T> setMediaType(final String mediaType) {
                this.mediaType = mediaType;
                return this;
            }

            public abstract T build() throws IOException;

        }

    }

    public static class FormDataTextPart extends FormDataPart {

        private final byte[] textData;

        FormDataTextPart(final Map<String, String> headers, final byte[] textData) {
            super(headers);

            this.textData = textData;
        }

        @Override
        public long getDataSize() {
            return textData != null ? textData.length : 0L;
        }

        @Override
        public void writeData(final OutputStream out) throws IOException {
            out.write(textData);
        }

        public static final class Builder extends FormDataPart.Builder<FormDataTextPart> {

            private String text;
            private String textCharset;

            public Builder() {
                super();
                this.text = null;
                this.textCharset = null;
            }

            public final Builder setText(final String text, final String textCharset) {
                this.text = text;
                this.textCharset = textCharset;
                return this;
            }

            @Override
            public final FormDataTextPart build() throws IOException {
                if (name == null || name.length() == 0) {
                    throw new RuntimeException("Missing form-data text part name");
                }

                final byte[] textData = text != null ? text.getBytes(textCharset != null ? textCharset : Http.CHARSET_UTF_8) : null;

                final Map<String, String> headers = new HashMap<>();

                headers.put(
                        Http.HEADER_CONTENT_DISPOSITION,
                        HttpUtil.buildHeader(
                                Http.CONTENT_DISPOSITION_FORM_DATA,
                                Collections.singletonMap(Http.HEADER_PARAM_NAME, String.format("\"%1$s\"", name))));

                headers.put(
                        Http.HEADER_CONTENT_TYPE,
                        HttpUtil.buildHeader(
                                mediaType != null ? mediaType : Http.MEDIA_TYPE_TEXT_PLAIN,
                                Collections.singletonMap(Http.HEADER_PARAM_CHARSET, textCharset != null ? textCharset : Http.CHARSET_UTF_8)));

                headers.put(Http.HEADER_CONTENT_LENGTH, String.valueOf(textData != null ? textData.length : 0));

                return new FormDataTextPart(headers, textData);
            }

        }

    }

    public static class FormDataFilePart extends FormDataPart {

        private static final int READ_BUFFER_SIZE = 1024 * 10;

        private InputStream fileInputStream;
        private long fileSize;

        FormDataFilePart(final Map<String, String> headers, final InputStream fileInputStream, final long fileSize) {
            super(headers);

            this.fileInputStream = fileInputStream;
            this.fileSize = fileSize;
        }

        @Override
        public long getDataSize() {
            return Math.max(fileSize, 0L);
        }

        @Override
        public void writeData(final OutputStream out) throws IOException {
            final byte[] readBuffer = new byte[READ_BUFFER_SIZE];

            int readCount;
            while ((readCount = fileInputStream.read(readBuffer)) != -1) {
                out.write(readBuffer, 0, readCount);
            }
        }

        public static final class Builder extends FormDataPart.Builder<FormDataFilePart> {

            private InputStream fileInputStream;
            private String fileName;
            private long fileSize;

            public Builder() {
                super();
                this.fileInputStream = null;
                this.fileName = null;
                this.fileSize = 0L;
            }

            public final Builder setFile(final InputStream fileInputStream, final String fileName, final long fileSize) {
                this.fileInputStream = fileInputStream;
                this.fileName = fileName;
                this.fileSize = Math.max(fileSize, 0L);
                return this;
            }

            @Override
            public final FormDataFilePart build() throws IOException {
                if (name == null || name.length() == 0) {
                    throw new RuntimeException("Missing form-data file part name");
                } else if (fileName == null || fileName.length() == 0) {
                    throw new RuntimeException("Missing form-data file part fileName");
                }

                final Map<String, String> headers = new HashMap<>();

                final Map<String, String> contentDispositionParams = new HashMap<>();
                contentDispositionParams.put(Http.HEADER_PARAM_NAME, String.format("\"%1$s\"", name));
                contentDispositionParams.put(Http.HEADER_PARAM_FILENAME, String.format("\"%1$s\"", fileName));

                headers.put(
                        Http.HEADER_CONTENT_DISPOSITION,
                        HttpUtil.buildHeader(
                                Http.CONTENT_DISPOSITION_FORM_DATA,
                                contentDispositionParams));

                headers.put(Http.HEADER_CONTENT_TYPE, mediaType != null ? mediaType : Http.MEDIA_TYPE_APP_OCTET_STREAM);
                headers.put(Http.HEADER_CONTENT_LENGTH, String.valueOf(fileSize));

                return new FormDataFilePart(headers, fileInputStream, fileSize);
            }

        }

    }

}
