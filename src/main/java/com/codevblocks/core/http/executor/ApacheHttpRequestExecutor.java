/*
 * ApacheHttpPostExecutor.java
 *
 * Created on Oct 22, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.executor;

import com.codevblocks.core.http.Http;
import com.codevblocks.core.http.HttpLog;
import com.codevblocks.core.http.HttpRawResponse;
import com.codevblocks.core.http.HttpRequest;
import com.codevblocks.core.http.auth.HttpAuth;
import com.codevblocks.core.http.auth.HttpBasicAuthCredentials;
import com.codevblocks.core.http.parameter.HttpParameter;
import org.apache.http.*;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class ApacheHttpRequestExecutor extends HttpRequestExecutor {

    private final HttpRequestBase mHttpRequest;

    public ApacheHttpRequestExecutor(final HttpRequestBase httpRequest, final HttpRequest request) {
        super(request);
        this.mHttpRequest = httpRequest;
    }

    @Override
    public HttpRawResponse execute() throws Throwable {
        // Get log instance
        final HttpLog logger = HttpLog.getInstance();

        // Get client request object
        final HttpRequest clientRequest = getRequest();

        // Log request
        logger.log(clientRequest);

        // Create an URI string builder and initialize it with the client request path
        final StringBuilder uriStringBuilder = new StringBuilder(clientRequest.getPath());

        // Check if client request has parameters and append them to the URI string builder
        final Collection<? extends HttpParameter> params = clientRequest.getParameters();
        if (params != null && params.size() > 0) {
            uriStringBuilder.append('?').append(HttpParameter.buildUriQuery(params));
        }

        // Build the request URI and set it to the HTTP method request
        mHttpRequest.setURI(URI.create(uriStringBuilder.toString()));

        // Check if client request has headers
        final Map<String, String> requestHeaders = clientRequest.getHeaders();
        if (requestHeaders != null && requestHeaders.size() > 0) {
            // Add client request header to HTTP method request
            for (Map.Entry<String, String> header : requestHeaders.entrySet()) {
                mHttpRequest.addHeader(header.getKey(), header.getValue());
            }
        }

        // Check if the HTTP method request supports output data
        if (mHttpRequest instanceof HttpEntityEnclosingRequest) {
            // Check if client request has data and add it to the HTTP method request
            final byte[] requestData = clientRequest.getData();
            if (requestData != null && requestData.length > 0) {
                ((HttpEntityEnclosingRequest) mHttpRequest).setEntity(new ByteArrayEntity(requestData));
            }
        }

        final HttpParams httpParams = new BasicHttpParams();

        // Set connection timeout
        final int connectionTimeout = clientRequest.getConnectionTimeout();
        HttpConnectionParams.setConnectionTimeout(httpParams, connectionTimeout);

        // Set read timeout
        final int readTimeout = clientRequest.getReadTimeout();
        HttpConnectionParams.setSoTimeout(httpParams, readTimeout);

        // Create an HTTP client
        final MeteredDefaultHttpClient httpClient = new MeteredDefaultHttpClient(httpParams);

        // Check if client request overrides the default trust managers
        final TrustManager[] trustManagers = clientRequest.getTrustManagers();
        if (trustManagers != null) {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustManagers, new java.security.SecureRandom());

            SSLSocketFactory socketFactory = new SSLSocketFactory(sc);
            Scheme sch = new Scheme("https", socketFactory, 443);
            httpClient.getConnectionManager().getSchemeRegistry().register(sch);
        }

        // Check if client request requires authentication
        final HttpAuth httpAuth = clientRequest.getAuthorization();
        if (httpAuth != null) {
            if (requestHeaders == null || !requestHeaders.containsKey(Http.HEADER_AUTHORIZATION)) {
                final HttpAuth.Type httpAuthType = httpAuth.getType();
                if (httpAuthType == HttpAuth.Type.BASIC) {
                    final HttpBasicAuthCredentials httpAuthCredentials = (HttpBasicAuthCredentials) httpAuth.getCredentials();
                    httpClient.getCredentialsProvider().setCredentials(AuthScope.ANY,
                            new UsernamePasswordCredentials(httpAuthCredentials.getName(), httpAuthCredentials.getPassword()));
                }
            }
        }

        // Execute HTTP method request
        final HttpResponse httpResponse = httpClient.execute(mHttpRequest);

        clientRequest.setBytesSent(httpClient.getBytesSent());
        clientRequest.setBytesReceived(httpClient.getByteReceived());

        // Retrieve the HTTP status line
        final StatusLine httpStatusLine = httpResponse.getStatusLine();

        // Create a client response
        final HttpRawResponse response = new HttpRawResponse();
        // Set client response code
        response.setCode(httpStatusLine.getStatusCode());
        // Set client response message
        response.setMessage(httpStatusLine.getReasonPhrase());

        // Check if HTTP response has headers
        final Header[] httpResponseHeaders = httpResponse.getAllHeaders();
        if (httpResponseHeaders != null && httpResponseHeaders.length > 0) {
            // Create client response headers map
            final Map<String, String> responseHeaders = new HashMap<String, String>();

            // Add HTTP response headers to client response headers map
            for (Header header : httpResponseHeaders) {
                responseHeaders.put(header.getName(), header.getValue());
            }

            // Set client response headers map
            response.setHeaders(responseHeaders);
        }

        // Check if HTTP response has data
        final HttpEntity httpResponseEntity = httpResponse.getEntity();
        if (httpResponseEntity != null) {
            // Get HTTP response data input stream
            final InputStream httpInputStream = httpResponseEntity.getContent();

            // Create a data output stream
            final ByteArrayOutputStream dataOutputStream = new ByteArrayOutputStream();

            // Allocate read buffer
            final byte[] buffer = new byte[1024];

            // Read response data from input stream and store it into data output stream
            int readCount;
            while ((readCount = httpInputStream.read(buffer)) != -1) {
                dataOutputStream.write(buffer, 0, readCount);
            }

            // Set client response data
            response.setData(dataOutputStream.toByteArray());
        }

        // Log response
        logger.log(response);

        return response;
    }

    private final class MeteredDefaultHttpClient extends DefaultHttpClient {

        private AtomicLong mBytesSent = new AtomicLong(0L);
        private AtomicLong mByteReceived = new AtomicLong(0L);

        public MeteredDefaultHttpClient() {
            super();
        }

        public MeteredDefaultHttpClient(HttpParams params) {
            super(params);
        }

        public MeteredDefaultHttpClient(final ClientConnectionManager conman, final HttpParams params) {
            super(conman, params);
        }

        @Override
        protected final org.apache.http.protocol.HttpRequestExecutor createRequestExecutor() {
            return new org.apache.http.protocol.HttpRequestExecutor() {

                @Override
                protected final HttpResponse doSendRequest(
                        final org.apache.http.HttpRequest request,
                        final HttpClientConnection conn,
                        final HttpContext context) throws IOException, HttpException {
                    final HttpResponse response = super.doSendRequest(request, conn, context);

                    final HttpConnectionMetrics connectionMetrics = conn.getMetrics();
                    mBytesSent.addAndGet(connectionMetrics != null ? connectionMetrics.getSentBytesCount() : 0);

                    return response;
                }

                @Override
                protected final HttpResponse doReceiveResponse(
                        final org.apache.http.HttpRequest request,
                        final HttpClientConnection conn,
                        final HttpContext context) throws HttpException, IOException {
                    final HttpResponse response = super.doReceiveResponse(request, conn, context);

                    final HttpConnectionMetrics connectionMetrics = conn.getMetrics();
                    mByteReceived.addAndGet(connectionMetrics != null ? connectionMetrics.getReceivedBytesCount() : 0);

                    return response;
                }

            };
        }

        public final long getBytesSent() {
            return mBytesSent.get();
        }

        public final long getByteReceived() {
            return mByteReceived.get();
        }

    }

}
