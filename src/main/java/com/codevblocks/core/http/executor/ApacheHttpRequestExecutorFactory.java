/*
 * ApacheHttpRequestExecutorFactory.java
 *
 * Created on Oct 21, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.executor;

import com.codevblocks.core.http.Http;
import com.codevblocks.core.http.HttpRequest;
import org.apache.http.client.methods.*;

import java.net.URI;

public class ApacheHttpRequestExecutorFactory extends HttpRequestExecutorFactory {

    @Override
    public HttpRequestExecutor newExecutor(HttpRequest request) {
        HttpRequestExecutor requestExecutor = super.newExecutor(request);

        if (requestExecutor == null) {
            return new ApacheHttpRequestExecutor(new HttpRequestMethod(request.getMethod()), request);
        }

        return requestExecutor;
    }

    @Override
    protected HttpRequestExecutor newGetExecutor(final HttpRequest request) {
        return new ApacheHttpRequestExecutor(new HttpGet(), request);
    }

    @Override
    protected HttpRequestExecutor newPostExecutor(final HttpRequest request) {
        return new ApacheHttpRequestExecutor(new HttpPost(), request);
    }

    @Override
    protected HttpRequestExecutor newPutExecutor(final HttpRequest request) {
        return new ApacheHttpRequestExecutor(new HttpPut(), request);
    }

    @Override
    protected HttpRequestExecutor newDeleteExecutor(final HttpRequest request) {
        return new ApacheHttpRequestExecutor(new HttpDelete(), request);
    }

    @Override
    protected HttpRequestExecutor newPatchExecutor(final HttpRequest request) {
        return new ApacheHttpRequestExecutor(new HttpRequestMethod(Http.PATCH), request);
    }

    @Override
    protected HttpRequestExecutor newHeadExecutor(HttpRequest request) {
        return new ApacheHttpRequestExecutor(new HttpHead(), request);
    }

    @Override
    protected HttpRequestExecutor newOptionsExecutor(HttpRequest request) {
        return new ApacheHttpRequestExecutor(new HttpOptions(), request);
    }

    public class HttpRequestMethod extends HttpEntityEnclosingRequestBase {

        private final String mMethod;

        public HttpRequestMethod(final String method) {
            super();
            this.mMethod = method;
        }

        public HttpRequestMethod(final String method, final URI uri) {
            super();
            this.mMethod = method;
            setURI(uri);
        }

        public HttpRequestMethod(final String method, final String uri) {
            super();
            this.mMethod = method;
            setURI(URI.create(uri));
        }

        @Override
        public String getMethod() {
            return mMethod;
        }
    }

}
