/*
 * DefaultHttpRequestExecutor.java
 *
 * Created on Oct 22, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.executor;

import com.codevblocks.core.http.Http;
import com.codevblocks.core.http.HttpLog;
import com.codevblocks.core.http.HttpRawResponse;
import com.codevblocks.core.http.HttpRequest;
import com.codevblocks.core.http.auth.HttpAuth;
import com.codevblocks.core.http.auth.HttpBasicAuthCredentials;
import com.codevblocks.core.http.io.MeteredInputStream;
import com.codevblocks.core.http.io.MeteredOutputStream;
import com.codevblocks.core.http.parameter.HttpParameter;
import org.apache.commons.codec.binary.Base64;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class DefaultHttpRequestExecutor extends HttpRequestExecutor {

    private final String mHttpRequestMethod;

    public DefaultHttpRequestExecutor(final String httpRequestMethod, final HttpRequest request) {
        super(request);
        this.mHttpRequestMethod = httpRequestMethod;
    }

    @Override
    public HttpRawResponse execute() throws Throwable {
        // Get log instance
        final HttpLog logger = HttpLog.getInstance();

        // Get client request object
        final HttpRequest clientRequest = getRequest();

        // Log request
        logger.log(clientRequest);

        // Create an URI string builder and initialize it with the client request path
        final StringBuilder uriStringBuilder = new StringBuilder(clientRequest.getPath());

        // Check if client request has parameters and append them to the URI string builder
        final Collection<? extends HttpParameter> params = clientRequest.getParameters();
        if (params != null && params.size() > 0) {
            uriStringBuilder.append('?').append(HttpParameter.buildUriQuery(params));
        }

        final URL requestUrl = new URI(uriStringBuilder.toString()).toURL();
        final HttpURLConnection connection = (HttpURLConnection) requestUrl.openConnection();

        // Set HTTP connection request method
        connection.setRequestMethod(mHttpRequestMethod);

        // Check if this is a secure connection
        if (connection instanceof HttpsURLConnection) {
            // Check if client request overrides the default trust managers
            final TrustManager[] trustManagers = clientRequest.getTrustManagers();

            if (trustManagers != null) {
                // Install client trust managers
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustManagers, new java.security.SecureRandom());
                ((HttpsURLConnection) connection).setSSLSocketFactory(sc.getSocketFactory());
            }
        }

        // Initialize connection for reading
        connection.setDoInput(true);

        // Check if client request requires authentication
        final HttpAuth httpAuth = clientRequest.getAuthorization();
        if (httpAuth != null) {
            final HttpAuth.Type httpAuthType = httpAuth.getType();
            if (httpAuthType == HttpAuth.Type.BASIC) {
                final HttpBasicAuthCredentials httpAuthCredentials = (HttpBasicAuthCredentials) httpAuth.getCredentials();
                final String credentialsString = String.format("%1$s:%2$s", httpAuthCredentials.getName(), httpAuthCredentials.getPassword());
                final String credentialsEncoded = new String(Base64.encodeBase64(credentialsString.getBytes()));
                connection.setRequestProperty(Http.HEADER_AUTHORIZATION, String.format("Basic %1$s", credentialsEncoded));
            }
        }

        // Check if client request has headers
        final Map<String, String> requestHeaders = clientRequest.getHeaders();
        if (requestHeaders != null && requestHeaders.size() > 0) {
            // Add client request headers to HTTP connection
            for (Map.Entry<String, String> header : requestHeaders.entrySet()) {
                connection.setRequestProperty(header.getKey(), header.getValue());
            }
        }

        // Check if client request has data and write it to the HTTP connection output
        final byte[] requestData = clientRequest.getData();
        if (requestData != null && requestData.length > 0) {
            // Initialize connection for writing
            connection.setDoOutput(true);

            // Write 'Content-Length' header
            connection.setRequestProperty(Http.HEADER_CONTENT_LENGTH, String.valueOf(requestData.length));

            // Write data to output stream
            OutputStream connectionOutputStream = null;
            MeteredOutputStream meteredOutputStream = null;
            try {
                connectionOutputStream = connection.getOutputStream();
                meteredOutputStream = connectionOutputStream != null ?
                        new MeteredOutputStream(connectionOutputStream) : null;

                if (meteredOutputStream != null) {
                    meteredOutputStream.write(requestData);
                }
            } finally {
                if (meteredOutputStream != null) {
                    clientRequest.setBytesSent(meteredOutputStream.getWriteCount());

                    try {
                        meteredOutputStream.close();
                    } catch (Throwable t) {
                        /* NOTHING TO DO */
                    }
                }
            }
        } else {
            connection.setDoOutput(false);
        }

        // Set connection timeout
        final int connectionTimeout = clientRequest.getConnectionTimeout();
        connection.setConnectTimeout(connectionTimeout);

        // Set read timeout
        final int readTimeout = clientRequest.getReadTimeout();
        connection.setReadTimeout(readTimeout);

        // Create a client response
        final HttpRawResponse response = new HttpRawResponse();
        // Set client response code
        response.setCode(connection.getResponseCode());
        // Set client response message
        response.setMessage(connection.getResponseMessage());

        // Check if HTTP response has headers
        final Map<String, List<String>> httpResponseHeaders = connection.getHeaderFields();
        if (httpResponseHeaders != null && httpResponseHeaders.size() > 0) {
            // Create client response headers map
            final Map<String, String> responseHeaders = new HashMap<String, String>();

            // Add HTTP response headers to client response headers map
            for (Map.Entry<String, List<String>> headerEntry : httpResponseHeaders.entrySet()) {
                responseHeaders.put(headerEntry.getKey(), connection.getHeaderField(headerEntry.getKey()));
            }

            // Set client response headers map
            response.setHeaders(responseHeaders);
        }

        // Check if HTTP response has data
        InputStream inputStream = null;
        InputStream connectionInputStream = null;
        MeteredInputStream meteredInputStream = null;

        try {
            // Get HTTP response data input stream
            if (response.getCode() / 100 == 2) {
                connectionInputStream = connection.getInputStream();
                meteredInputStream = connectionInputStream != null ?
                        new MeteredInputStream(connectionInputStream) : null;

                if (connectionInputStream != null) {
                    if (Http.ENCODING_GZIP.equalsIgnoreCase(connection.getContentEncoding())) {
                        inputStream = new GZIPInputStream(meteredInputStream);
                    } else {
                        inputStream = meteredInputStream;
                    }
                }
            } else {
                connectionInputStream = connection.getErrorStream();
                meteredInputStream = connectionInputStream != null ?
                        new MeteredInputStream(connectionInputStream) : null;

                inputStream = meteredInputStream;
            }

            if (inputStream != null) {
                // Create a data output stream
                final ByteArrayOutputStream dataOutputStream = new ByteArrayOutputStream();

                // Allocate read buffer
                final byte[] buffer = new byte[1024];

                // Read response data from input stream and store it into data output stream
                int readCount = 0;
                while ((readCount = inputStream.read(buffer)) != -1) {
                    dataOutputStream.write(buffer, 0, readCount);
                }

                // Set client response data
                response.setData(dataOutputStream.size() > 0 ? dataOutputStream.toByteArray() : null);
            }
        } finally {
            if (inputStream != null) {
                if (meteredInputStream != null) {
                    clientRequest.setBytesReceived(meteredInputStream.getReadCount());
                }

                try {
                    inputStream.close();
                } catch (Throwable t) {
                    /* NOTHING TO DO */
                }
            }
        }

        // Close connection
        connection.disconnect();

        // Log response
        logger.log(response);

        return response;
    }

}
