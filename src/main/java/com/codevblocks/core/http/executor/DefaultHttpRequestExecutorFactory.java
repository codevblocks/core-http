/*
 * HttpRequestExecutorFactory.java
 *
 * Created on Oct 16, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.executor;

import com.codevblocks.core.http.Http;
import com.codevblocks.core.http.HttpRequest;

public class DefaultHttpRequestExecutorFactory extends HttpRequestExecutorFactory {

    @Override
    protected HttpRequestExecutor newGetExecutor(final HttpRequest request) {
        return new DefaultHttpRequestExecutor(Http.GET, request);
    }

    @Override
    protected HttpRequestExecutor newPostExecutor(final HttpRequest request) {
        return new DefaultHttpRequestExecutor(Http.POST, request);
    }

    @Override
    protected HttpRequestExecutor newPutExecutor(final HttpRequest request) {
        return new DefaultHttpRequestExecutor(Http.PUT, request);
    }

    @Override
    protected HttpRequestExecutor newDeleteExecutor(final HttpRequest request) {
        return new DefaultHttpRequestExecutor(Http.DELETE, request);
    }

    @Override
    protected HttpRequestExecutor newPatchExecutor(final HttpRequest request) {
        return new DefaultHttpRequestExecutor(Http.PATCH, request);
    }

    @Override
    protected HttpRequestExecutor newHeadExecutor(final HttpRequest request) {
        return new DefaultHttpRequestExecutor(Http.HEAD, request);
    }

    @Override
    protected HttpRequestExecutor newOptionsExecutor(final HttpRequest request) {
        return new DefaultHttpRequestExecutor(Http.OPTIONS, request);
    }

}
