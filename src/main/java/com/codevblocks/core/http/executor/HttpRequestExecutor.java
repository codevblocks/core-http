/*
 * HttpRequestExecutor.java
 *
 * Created on Oct 16, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.executor;

import com.codevblocks.core.http.HttpRawResponse;
import com.codevblocks.core.http.HttpRequest;

public abstract class HttpRequestExecutor {

    private final HttpRequest mRequest;

    public HttpRequestExecutor(final HttpRequest request) {
        this.mRequest = request;
    }

    protected final HttpRequest getRequest() {
        return mRequest;
    }

    public abstract HttpRawResponse execute() throws Throwable;

}
