/*
 * HttpRequestExecutorFactory.java
 *
 * Created on Oct 16, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.executor;

import com.codevblocks.core.http.Http;
import com.codevblocks.core.http.HttpRequest;

public class HttpRequestExecutorFactory {

    private enum HttpMethodHash {
        GET(Http.GET.hashCode()),
        POST(Http.POST.hashCode()),
        PUT(Http.PUT.hashCode()),
        DELETE(Http.DELETE.hashCode()),
        PATCH(Http.PATCH.hashCode()),
        HEAD(Http.HEAD.hashCode()),
        OPTIONS(Http.OPTIONS.hashCode());

        public final int hash;

        private HttpMethodHash(final int hash) {
            this.hash = hash;
        }

        public static final HttpMethodHash findByHash(final int hash) {
            for (HttpMethodHash httpMethodHash : values()) {
                if (httpMethodHash.hash == hash) {
                    return httpMethodHash;
                }
            }

            return null;
        }

    }

    private static HttpRequestExecutorFactory sDefaultFactory = new DefaultHttpRequestExecutorFactory();

    public static final void setDefault(final HttpRequestExecutorFactory factory) {
        if (factory != null) {
            synchronized (HttpRequestExecutorFactory.class) {
                sDefaultFactory = factory;
            }
        }
    }

    public static final HttpRequestExecutorFactory getDefault() {
        return sDefaultFactory;
    }

    public HttpRequestExecutor newExecutor(final HttpRequest request) {
        switch (HttpMethodHash.findByHash(request.getMethod().hashCode())) {
            case GET: { return newGetExecutor(request); }
            case POST: { return newPostExecutor(request); }
            case PUT: { return newPutExecutor(request); }
            case DELETE: { return newDeleteExecutor(request); }
            case PATCH: { return newPatchExecutor(request); }
            case HEAD: { return newHeadExecutor(request); }
            case OPTIONS: { return newOptionsExecutor(request); }
        }

        return null;
    }

    protected HttpRequestExecutor newGetExecutor(final HttpRequest request) {
        return null;
    }

    protected HttpRequestExecutor newPostExecutor(final HttpRequest request) {
        return null;
    }

    protected HttpRequestExecutor newPutExecutor(final HttpRequest request) {
        return null;
    }

    protected HttpRequestExecutor newDeleteExecutor(final HttpRequest request) {
        return null;
    }

    protected HttpRequestExecutor newPatchExecutor(final HttpRequest request) {
        return null;
    }

    protected HttpRequestExecutor newHeadExecutor(final HttpRequest request) {
        return null;
    }

    protected HttpRequestExecutor newOptionsExecutor(final HttpRequest request) {
        return null;
    }

}
