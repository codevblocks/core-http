/*
 * OkHttpRequestExecutor.java
 *
 * Created on Jan 8, 2020
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.executor;

import com.codevblocks.core.http.Http;
import com.codevblocks.core.http.HttpLog;
import com.codevblocks.core.http.HttpRawResponse;
import com.codevblocks.core.http.HttpRequest;
import com.codevblocks.core.http.auth.HttpAuth;
import com.codevblocks.core.http.auth.HttpBasicAuthCredentials;
import com.codevblocks.core.http.parameter.HttpParameter;
import okhttp3.*;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class OkHttpRequestExecutor extends HttpRequestExecutor {

    private final OkHttpClient.Builder mClientBuilder;
    private final String mRequestMethod;

    public OkHttpRequestExecutor(final OkHttpClient.Builder clientBuilder, final String requestMethod, final HttpRequest request) {
        super(request);
        this.mClientBuilder = clientBuilder;
        this.mRequestMethod = requestMethod;
    }

    @Override
    public HttpRawResponse execute() throws Throwable {
        // Get log instance
        final HttpLog logger = HttpLog.getInstance();

        // Get client request object
        final HttpRequest clientRequest = getRequest();

        // Log request
        logger.log(clientRequest);

        // Prepare request builder
        final Request.Builder requestBuilder = new Request.Builder();

        // Create an URI string builder and initialize it with the client request path
        final StringBuilder uriStringBuilder = new StringBuilder(clientRequest.getPath());

        // Check if client request has parameters and append them to the URI string builder
        final Collection<? extends HttpParameter> params = clientRequest.getParameters();
        if (params != null && params.size() > 0) {
            uriStringBuilder.append('?').append(HttpParameter.buildUriQuery(params));
        }

        final URL requestUrl = new URI(uriStringBuilder.toString()).toURL();
        requestBuilder.url(requestUrl);

        // Check if client request requires authentication
        final HttpAuth httpAuth = clientRequest.getAuthorization();
        if (httpAuth != null) {
            final HttpAuth.Type httpAuthType = httpAuth.getType();
            if (httpAuthType == HttpAuth.Type.BASIC) {
                final HttpBasicAuthCredentials credentials = (HttpBasicAuthCredentials) httpAuth.getCredentials();
                requestBuilder.addHeader(Http.HEADER_AUTHORIZATION, Credentials.basic(credentials.getName(), credentials.getPassword()));
            }
        }

        // Check if client request has headers
        final Map<String, String> requestHeaders = clientRequest.getHeaders();
        if (requestHeaders != null && requestHeaders.size() > 0) {
            // Add client request header to HTTP method request
            for (Map.Entry<String, String> header : requestHeaders.entrySet()) {
                requestBuilder.addHeader(header.getKey(), header.getValue());
            }
        }

        // Check if client request overrides the default trust managers
        final TrustManager[] trustManagers = clientRequest.getTrustManagers();
        if (trustManagers != null) {
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustManagers, new java.security.SecureRandom());

            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            mClientBuilder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustManagers[0]);
        }

        RequestBody requestBody = null;

        // Check if client request has data and create a request body
        final byte[] requestData = clientRequest.getData();
        if (requestData != null && requestData.length > 0) {
            final String contentTypeHeader = requestHeaders != null ? requestHeaders.get(Http.HEADER_CONTENT_TYPE) : null;
            requestBody = RequestBody.create(MediaType.parse(contentTypeHeader), requestData);

            requestBuilder.addHeader(Http.HEADER_CONTENT_LENGTH, String.valueOf(requestBody.contentLength()));
        }

        // Set HTTP connection request method and request body
        requestBuilder.method(mRequestMethod, requestBody);

        // Set connection timeout
        final int connectionTimeout = clientRequest.getConnectionTimeout();
        mClientBuilder.connectTimeout(connectionTimeout, TimeUnit.MILLISECONDS);

        // Set read timeout
        final int readTimeout = clientRequest.getReadTimeout();
        mClientBuilder.readTimeout(readTimeout, TimeUnit.MILLISECONDS);

        // Create an HTTP client
        final OkHttpClient httpClient = mClientBuilder.build();

        // Build HTTP request
        final Request httpRequest = requestBuilder.build();

        // Execute HTTP request
        final Response httpResponse = httpClient.newCall(httpRequest).execute();

        // Create a client response
        final HttpRawResponse response = new HttpRawResponse();
        // Set client response code
        response.setCode(httpResponse.code());
        // Set client response message
        response.setMessage(httpResponse.message());

        // Check if HTTP response has headers
        final Headers responseHeaders = httpResponse.headers();
        if (responseHeaders.size() > 0) {
            // Create client response headers map
            final Map<String, String> responseHeadersMap = new HashMap<String, String>();

            // Add HTTP response headers to client response headers map
            final Set<String> headerNames = responseHeaders.names();
            for (String headerName : headerNames) {
                responseHeadersMap.put(headerName, responseHeadersMap.get(headerName));
            }

            // Set client response headers map
            response.setHeaders(responseHeadersMap);
        }

        final ResponseBody responseBody = httpResponse.body();
        if (responseBody != null) {
            response.setData(responseBody.bytes());
        }

        clientRequest.setBytesSent(httpRequest.headers().byteCount() + (requestBody != null ? requestBody.contentLength() : 0));
        clientRequest.setBytesReceived(responseHeaders.byteCount() + (responseBody != null ? responseBody.contentLength() : 0));

        // Log response
        logger.log(response);

        return response;
    }

}
