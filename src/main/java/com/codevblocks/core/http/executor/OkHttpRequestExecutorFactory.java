/*
 * OkHttpRequestExecutorFactory.java
 *
 * Created on Jan 8, 2020
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.executor;

import com.codevblocks.core.http.Http;
import com.codevblocks.core.http.HttpRequest;
import okhttp3.OkHttpClient;

public class OkHttpRequestExecutorFactory extends HttpRequestExecutorFactory {

    private final OkHttpClient mHttpClient;

    public OkHttpRequestExecutorFactory() {
        this(new OkHttpClient());
    }

    public OkHttpRequestExecutorFactory(final OkHttpClient httpClient) {
        this.mHttpClient = httpClient;
    }

    @Override
    protected HttpRequestExecutor newGetExecutor(final HttpRequest request) {
        return new OkHttpRequestExecutor(mHttpClient.newBuilder(), Http.GET, request);
    }

    @Override
    protected HttpRequestExecutor newPostExecutor(final HttpRequest request) {
        return new OkHttpRequestExecutor(mHttpClient.newBuilder(), Http.POST, request);
    }

    @Override
    protected HttpRequestExecutor newPutExecutor(final HttpRequest request) {
        return new OkHttpRequestExecutor(mHttpClient.newBuilder(), Http.PUT, request);
    }

    @Override
    protected HttpRequestExecutor newDeleteExecutor(final HttpRequest request) {
        return new OkHttpRequestExecutor(mHttpClient.newBuilder(), Http.DELETE, request);
    }

    @Override
    protected HttpRequestExecutor newPatchExecutor(final HttpRequest request) {
        return new OkHttpRequestExecutor(mHttpClient.newBuilder(), Http.PATCH, request);
    }

    @Override
    protected HttpRequestExecutor newHeadExecutor(final HttpRequest request) {
        return new OkHttpRequestExecutor(mHttpClient.newBuilder(), Http.HEAD, request);
    }

    @Override
    protected HttpRequestExecutor newOptionsExecutor(final HttpRequest request) {
        return new OkHttpRequestExecutor(mHttpClient.newBuilder(), Http.OPTIONS, request);
    }

}
