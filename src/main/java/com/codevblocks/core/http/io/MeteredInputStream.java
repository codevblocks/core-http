package com.codevblocks.core.http.io;

import java.io.IOException;
import java.io.InputStream;

public class MeteredInputStream extends InputStream {

    private final InputStream mInputStream;
    private long mReadCount;

    public MeteredInputStream(final InputStream inputStream) {
        this.mInputStream = inputStream;
        mReadCount = 0;
    }

    @Override
    public int read() throws IOException {
        final int read = mInputStream.read();
        mReadCount += read != -1 ? 1 : 0;
        return read;
    }

    @Override
    public int read(final byte[] buffer) throws IOException {
        return this.read(buffer, 0, buffer.length);
    }

    @Override
    public int read(final byte[] buffer, final int byteOffset, final int byteCount) throws IOException {
        final int readCount = mInputStream.read(buffer, byteOffset, byteCount);
        mReadCount += readCount != -1 ? readCount : 0;
        return readCount;
    }

    public final long getReadCount() {
        return mReadCount;
    }

    public final void clearReadCount() {
        mReadCount = 0;
    }

}
