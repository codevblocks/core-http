package com.codevblocks.core.http.io;

import java.io.IOException;
import java.io.OutputStream;

public class MeteredOutputStream extends OutputStream {

    private final OutputStream mOutputStream;
    private long mWriteCount;

    public MeteredOutputStream(final OutputStream outputStream) {
        this.mOutputStream = outputStream;
    }

    @Override
    public void write(final int b) throws IOException {
        mOutputStream.write(b);
        ++mWriteCount;
    }

    @Override
    public void write(final byte[] buffer) throws IOException {
        this.write(buffer, 0, buffer.length);
    }

    @Override
    public void write(final byte[] buffer, final int byteOffset, final int byteCount) throws IOException {
        mOutputStream.write(buffer, byteOffset, byteCount);
        mWriteCount += byteCount;
    }

    public final long getWriteCount() {
        return mWriteCount;
    }

    public final void clearWriteCount() {
        mWriteCount = 0;
    }

}
