package com.codevblocks.core.http.io;

import java.io.IOException;
import java.io.OutputStream;

public final class VoidOutputStream extends OutputStream {

    @Override
    public final void write(final int b) throws IOException {}

    @Override
    public final void write(final byte[] b) throws IOException {}

    @Override
    public final void write(final byte[] b, final int off, final int len) throws IOException {}

}
