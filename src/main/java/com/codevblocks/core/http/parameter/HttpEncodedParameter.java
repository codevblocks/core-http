/*
 * HttpParameterEncoded.java
 *
 * Created on Oct 22, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.parameter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class HttpEncodedParameter extends HttpParameter {

    public HttpEncodedParameter(final String name, final String value, final String encoding) {
        super(name, encode(value, encoding));
    }

    private static final String encode(final String value, final String encoding) {
        try {
            return (value != null ? URLEncoder.encode(value, encoding) : null);
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }

}
