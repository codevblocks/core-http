/*
 * HttpParameter.java
 *
 * Created on Oct 16, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.parameter;

import java.util.Collection;

public class HttpParameter {

    public final String name;
    public final String value;

    public HttpParameter(final String name, final String value) {
        this.name = name;
        this.value = value;
    }

    public static final String buildUriQuery(final HttpParameter... params) {
        if (params == null || params.length == 0) {
            return "";
        }

        final StringBuilder queryStringBuilder = new StringBuilder();

        for (HttpParameter param : params) {
            if (param != null && param.name != null && param.name.length() > 0 && param.value != null) {
                if (queryStringBuilder.length() > 0) {
                    queryStringBuilder.append('&');
                }

                queryStringBuilder.append(param.name).append('=').append(param.value);
            }
        }

        return queryStringBuilder.toString();
    }

    public static final String buildUriQuery(final Collection<? extends HttpParameter> params) {
        if (params == null || params.size() == 0) {
            return "";
        }

        final StringBuilder queryStringBuilder = new StringBuilder();

        for (HttpParameter param : params) {
            if (param != null && param.name != null && param.name.length() > 0 && param.value != null) {
                if (queryStringBuilder.length() > 0) {
                    queryStringBuilder.append('&');
                }

                queryStringBuilder.append(param.name).append('=').append(param.value);
            }
        }

        return queryStringBuilder.toString();
    }

    public static final String buildSeparatedValues(final Object[] values, final String separator) {
        if (values == null || values.length == 0) {
            return null;
        }

        final StringBuilder valuesBuilder = new StringBuilder();

        for (int i = 0 ; i < values.length ; ++i) {
            if (i > 0) {
                valuesBuilder.append(separator);
            }
            valuesBuilder.append(values[i]);
        }

        return valuesBuilder.toString();
    }

}
