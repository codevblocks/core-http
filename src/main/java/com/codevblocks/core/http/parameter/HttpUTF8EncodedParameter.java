/*
 * HttpUTF8EncodedParameter.java
 *
 * Created on Oct 22, 2013
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.parameter;

public class HttpUTF8EncodedParameter extends HttpEncodedParameter {

    private static final String UTF8_ENCODING = "UTF-8";

    public HttpUTF8EncodedParameter(final String name, final String value) {
        super(name, value, UTF8_ENCODING);
    }

}
