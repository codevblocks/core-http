/*
 * HttpUtil.java
 *
 * Created on Jan 8, 2020
 * Author: Cosmin Radu
 */

package com.codevblocks.core.http.util;

import java.util.Map;
import java.util.Random;

public final class HttpUtil {

    private static final char[] MULTIPART_BOUNDARY_CHARS = new char[] {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
            'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    };

    private HttpUtil() {}

    public static final String randomMultipartBoundary(final int length) {
        final Random random = new Random(System.currentTimeMillis());

        final int count = Math.max(Math.min(70, length), 1);

        final StringBuilder boundary = new StringBuilder(count);
        for (int i = 0 ; i < count ; ++i) {
            boundary.append(MULTIPART_BOUNDARY_CHARS[random.nextInt(MULTIPART_BOUNDARY_CHARS.length)]);
        }

        return boundary.toString();
    }

    public static final String parseHeaderMediaType(final String header) {
        final String[] headerSegments = header != null ? header.split(";") : null;
        return headerSegments != null && headerSegments.length > 0 ? headerSegments[0].trim() : null;
    }

    public static final String parseHeaderParamValue(final String header, final String param) {
        final String[] headerSegments = header != null ? header.split(";") : null;
        final int headerSegmentsCount = headerSegments != null ? headerSegments.length : 0;

        if (headerSegmentsCount > 1) {
            String segment;
            String[] segmentParams;
            for (int i = 1 ; i < headerSegmentsCount ; ++i) {
                segment = headerSegments[i].trim();
                segmentParams = segment.split("=");
                if (segmentParams != null && segmentParams.length == 2) {
                    if (segmentParams[0].toLowerCase().equals(param.toLowerCase())) {
                        return segmentParams[1];
                    }
                }
            }
        }

        return null;
    }

    public static final String buildHeader(final String value, final Map<String, String> params) {
        final StringBuilder stringBuilder = new StringBuilder(value);

        if (params != null && params.size() > 0) {
            String paramKey;
            boolean hasKey;

            String paramValue;
            boolean hasValue;

            for (Map.Entry<String, String> paramEntry : params.entrySet()) {
                stringBuilder.append("; ");

                paramKey = paramEntry.getKey();
                hasKey = paramKey != null && paramKey.length() > 0;

                paramValue = paramEntry.getValue();
                hasValue = paramValue != null && paramValue.length() > 0;

                if (hasKey) {
                    stringBuilder.append(paramKey);

                    if (hasValue) {
                        stringBuilder.append("=").append(paramValue);
                    }
                }
            }
        }

        return stringBuilder.toString();
    }

}
